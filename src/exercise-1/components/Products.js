import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Products extends Component {
    render() {
        return (
            <div>
                <p>All Products:</p>
                <ul>
                    <li>
                        <Link to="/products/1" className="red">Bicyle</Link>
                    </li>
                    <li>
                        <Link to="/products/2" className="red">TV</Link>
                    </li>
                    <li>
                        <Link to="/products/3" className="red">Pencil</Link>
                    </li>
                </ul>
            </div>
        );
    }
}


export default Products;
