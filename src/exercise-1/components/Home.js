import React from 'react';

const Home = (props) => {
    return (
        <div>
            <p>This is a beautiful Home Page.</p>
            <p>And this url is "{ props.location.pathname }".</p>
        </div>
    );
}

export default Home;