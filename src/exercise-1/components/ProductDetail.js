import React, { Component } from 'react';
import data from '../../exercise-2/mockups/data.json';

export class ProductDetail extends Component {
    render() {
        let id = +this.props.match.params.id;
        let item = Object.values(data).find(item => item.id === id);
        
        console.log(Object.values(data));
        return ( 
            <div>
                <p>Product Details:</p>
                <p>Name: {item.name}</p>
                <p>Id: {item.id}</p>
                <p>Price: {item.price}</p>
                <p>Quantity: {item.desc}</p>
                <p>URL: {this.props.match.url}</p>
            </div>
        );
    }
}

export default ProductDetail;
