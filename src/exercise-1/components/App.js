import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import MyProfile from './MyProfile';
import AboutUs from './AboutUs';
import Home from './Home';
import Products from './Products';
import ProductDetail from './ProductDetail';
import '../styles/App.css'

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/products">Products</Link>
                </li>
                <li>
                  <Link to="/my-profile">My Profile</Link>
                </li>
                <li>
                  <Link to="/about-us">About Us</Link>
                </li>
              </ul>
            </nav>

            <div className='wrapper'>
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path={["/products", "/goods"]} exact component={Products} />
                <Route path="/my-profile" exact component={MyProfile} />
                <Route path="/about-us" exact component={AboutUs} />
                <Route path='/products/:id' component={ProductDetail}/>
                <Route component={Home}/>
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
