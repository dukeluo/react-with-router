import React from 'react';
import { Link } from 'react-router-dom';

const AboutUs = (props) => {
    return (
        <div>
            <p>Company: ThoughtWorks Local</p>
            <p>Location: Xi'an</p>
            <br/>
            <p>For more information, please</p>
            <p>view our <Link to="/" className="red">website</Link> </p>
      </div>

    );
}

export default AboutUs;